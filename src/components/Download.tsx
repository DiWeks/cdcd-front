import React from "react";
import { useState } from "react";
import axios from "axios";

interface DownloadProps {
  filekey: string | null;
}

const DownloadFile: React.FC<DownloadProps> = (props) => {
  // TODO stocker toutes les url d'images qu'on upload dans un state, au moins si on quitte l'app, et qu'on la rouvre ca y sera
  // toujours ??

  const [url, setUrl] = useState("");

  const { filekey } = props;

  async function fetchData() {
    const { data } = await axios.get(`http://localhost:3001/server/download/${filekey}`);

    setUrl(data);
  }

  if (filekey != "") {
    fetchData();
  }

  return <>{url && <img src={url} alt="My Image"></img>}</>;
};

export default DownloadFile;