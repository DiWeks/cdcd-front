//Libraires
import React from "react";

//Components
import { NavLink } from "react-router-dom";
import { List, ListItem, Center } from "@chakra-ui/react";

//Hooks
import { useContext } from "react";
import { useLocation } from "react-router-dom";

//Context
import { ThemeContext } from "../../../contexts/theme";

//Icons
import {
  IoAddCircleOutline,
  IoAddCircleSharp,
  IoChatbubbleOutline,
  IoChatbubbleSharp,
  IoHomeOutline,
  IoHomeSharp,
  IoPeopleOutline,
  IoPeopleSharp,
  IoPersonCircleOutline,
  IoPersonCircleSharp,
} from "react-icons/io5";

//Routes
import routes from "../../../config/routes";

interface IconProps {
  route: string;
  outline: JSX.Element;
  fill: JSX.Element;
  label: string;
}

const BottomNavigation = () => {
  const theme = useContext(ThemeContext);
  let location = useLocation();

  const tabs: IconProps[] = [
    {
      route: routes.HOME,
      outline: <IoHomeOutline size="24px" color={theme.secondary} />,
      fill: <IoHomeSharp size="24px" color={theme.secondary} />,
      label: "Accueil",
    },
    {
      route: routes.FRIENDS,
      outline: <IoPeopleOutline size="24px" color={theme.secondary} />,
      fill: <IoPeopleSharp size="24px" color={theme.secondary} />,
      label: "Amis",
    },
    {
      route: routes.ADD_POST,
      outline: <IoAddCircleOutline size="40px" color={theme.secondary} />,
      fill: <IoAddCircleSharp size="40px" color={theme.secondary} />,
      label: "Ajouter",
    },
    {
      route: routes.CHAT_LIST,
      outline: <IoChatbubbleOutline size="24px" color={theme.secondary} />,
      fill: <IoChatbubbleSharp size="24px" color={theme.secondary} />,
      label: "Messages",
    },
    {
      route: routes.PROFILE,
      outline: <IoPersonCircleOutline size="24px" color={theme.secondary} />,
      fill: <IoPersonCircleSharp size="24px" color={theme.secondary} />,
      label: "Réglages",
    },
  ];

  return (
    <nav style={{ position: "sticky", bottom: 0 }}>
      <List backgroundColor={theme.primary}>
        <Center h="50px" justifyContent="space-around">
          {tabs.map((tab, index) => (
            <ListItem key={`tab-${index}`}>
              <NavLink to={tab.route} className="nav-link">
                <Center flexDirection="column">
                  {location.pathname == tab.route ? tab.fill : tab.outline}
                </Center>
              </NavLink>
            </ListItem>
          ))}
        </Center>
      </List>
    </nav>
  );
};

export default BottomNavigation;
