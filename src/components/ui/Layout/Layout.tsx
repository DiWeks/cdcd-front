//Libraries
import React from "react";

//Components
import { Flex, Box } from "@chakra-ui/react";
import BottomNavigation from "../../navigation/BottomNavigation/BottomNavigation";
import Header from "../Header/Header";

//Hooks
import { useContext } from "react";
import { useLocation } from "react-router";

//Context
import { ThemeContext } from "../../../contexts/theme";

interface LayoutProps {
  children: JSX.Element;
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  const { pathname } = useLocation();
  const theme = useContext(ThemeContext);
  return (
    <Flex h="100vh" direction="column">
      <Header
        color={pathname !== "/signup" && pathname !== "/login" ? theme.primary : ""}
        logoSize={pathname !== "/signup" && pathname !== "/login" ? "50px" : "100px"}
      />
      <Box flex={1}>{children}</Box>
      {pathname !== "/signup" && pathname !== "/login" && <BottomNavigation />}
    </Flex>
  );
};

export default Layout;
