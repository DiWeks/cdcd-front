//Libraries
import React, { useEffect, useState } from "react";

import {
  Input,
  Box,
  Text,
  Button,
  InputGroup,
  InputRightElement
} from "@chakra-ui/react";

import { useContext } from "react";
import { ThemeContext } from "../../../contexts/theme";
import { IoSend } from "react-icons/io5";

interface BubbleChatProps { 
  messageList: any,
  username: string
}



const BubbleChat: React.FC<BubbleChatProps> = ({ messageList, username }) => {

  return (
    <Box>
      {messageList.map((messageContent: any, index: number) => {
        return (
          <Box key={index} >
            <Box>
              <Box>
                {messageContent.message}
              </Box>
              <Box>
                <Text> {messageContent.time} </Text>
                <Text> {messageContent.author} </Text>
              </Box>
            </Box>
          </Box>
        )
      })}
    </Box>
  );
}

export default BubbleChat;  