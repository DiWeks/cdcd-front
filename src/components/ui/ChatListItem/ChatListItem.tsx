//Libraries
import React from "react";

//Components
import { Flex, Avatar, Heading, Text } from "@chakra-ui/react";

//Hooks
import { useContext } from "react";
import { useNavigate } from "react-router";

//Context
import { ThemeContext } from "../../../contexts/theme";

//Routes
import routes from "../../../config/routes";

interface ChatListItemProps {
  id: number;
}

const ChatListItem: React.FC<ChatListItemProps> = ({ id }) => {
  const theme = useContext(ThemeContext);
  const navigate = useNavigate();

  //Methods
  const handleClick = () => {
    navigate(routes.CHAT_LIST + "/" + id);
  };

  return (
    <Flex onClick={handleClick} py={3} align="center" gap={3}>
      <Avatar
        size="md"
        name="Timothée Techer"
        src="https://media.licdn.com/dms/image/C4E03AQHg8NmZ1yxtWA/profile-displayphoto-shrink_800_800/0/1652307186402?e=2147483647&v=beta&t=UFT6cJvunLjT1q9Uxy3cq3KWF6_J9fv8lT9ra_hL-40"
      />

      <Flex justify="center" direction="column">
        <Heading as="h6" size="sm">
          Timothée Techer
        </Heading>
        <Text noOfLines={1}>
          Salut comment ça va dfesfrf reggre gregr regre etgre greg?
        </Text>
      </Flex>
    </Flex>
  );
};

export default ChatListItem;
