//LIbraries
import React from "react";

//Components
import { Card, CardBody, Text, Avatar, Flex, Box } from "@chakra-ui/react";

//Hooks
import { useContext } from "react";

//Contexts
import { ThemeContext } from "../../../../contexts/theme";

//Icons
import { IoCloseCircleOutline, IoCheckmarkCircleSharp } from "react-icons/io5";
interface FriendRequestCardProps {}

const FriendRequestCard: React.FC<FriendRequestCardProps> = () => {
  const theme = useContext(ThemeContext);
  return (
    <Card w="100%">
      <CardBody>
        <Flex align="center" justify="space-between">
          <Flex align="center" gap={2}>
            <Avatar
              size="sm"
              name="Timothée Techer"
              src="https://media.licdn.com/dms/image/C4E03AQHg8NmZ1yxtWA/profile-displayphoto-shrink_800_800/0/1652307186402?e=2147483647&v=beta&t=UFT6cJvunLjT1q9Uxy3cq3KWF6_J9fv8lT9ra_hL-40"
            />
            <Text>Timothée Techer</Text>
          </Flex>

          <Flex align="center" gap={4}>
            <IoCheckmarkCircleSharp color={theme.primary} size={22} />
            <IoCloseCircleOutline color={theme.error} size={22} />
          </Flex>
        </Flex>
      </CardBody>
    </Card>
  );
};

export default FriendRequestCard;
