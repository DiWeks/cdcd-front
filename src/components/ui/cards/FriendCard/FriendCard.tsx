//Libraries
import React from "react";

//Components
import { Card, CardBody, Text, Avatar, Flex, Box } from "@chakra-ui/react";

//Hooks
import { useContext } from "react";

//Contexts
import { ThemeContext } from "../../../../contexts/theme";

//Icons
import { IoTrashOutline } from "react-icons/io5";

interface FriendCardProps {}

const FriendCard: React.FC<FriendCardProps> = () => {
  const theme = useContext(ThemeContext);
  return (
    <Card w="100%">
      <CardBody>
        <Flex align="center" justify="space-between">
          <Flex align="center" gap={2}>
            <Avatar
              size="sm"
              name="Timothée Techer"
              src="https://media.licdn.com/dms/image/C4E03AQHg8NmZ1yxtWA/profile-displayphoto-shrink_800_800/0/1652307186402?e=2147483647&v=beta&t=UFT6cJvunLjT1q9Uxy3cq3KWF6_J9fv8lT9ra_hL-40"
            />
            <Text>Timothée Techer</Text>
          </Flex>

          <Box>
            <IoTrashOutline color={theme.error} size={22} />
          </Box>
        </Flex>
      </CardBody>
    </Card>
  );
};

export default FriendCard;
