import React, {useEffect} from 'react';
import socket from "../constants/socket"

const useSocketSetup = () => {
    const test = "test";
    useEffect(() => {
        socket.connect();
        socket.on("Connect_error", () => {
            // deconnecter user, il faut penser à l'implémenter
            console.log("SOCKET CONNECT ERROR")
        });
        return () => {
            socket.off("Connect error");
        };
    }, [test]);
};

export default useSocketSetup;