export interface Colors {
  primary: string;
  secondary: string;
  tertiary: string;
  text: string;
  text2: string;
  background: string;
  error: string;
}

export const light: Colors = {
  primary: "#41AB9E",
  secondary: "#0a3833",
  tertiary: "",
  text: "#000000",
  text2: "#B7B6B6",
  background: "#FFFFFF",
  error: "#F31C1C"
};

export const dark: Colors = {
  primary: "#41AB9E",
  secondary: "#0a3833",
  tertiary: "",
  text: "#FFFFFF",
  text2: "#B7B6B6",
  background: "#000000",
  error: "#F31C1C",
};
