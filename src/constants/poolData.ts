const userPoolId = process.env.REACT_APP_USERPOOLID;
if (!userPoolId) {
  throw new Error("UserPoolId is missing in environment variables");
}

const clientId = process.env.REACT_APP_CLIENTID;
if (!clientId) {
  throw new Error("ClientId is missing in environment variables");
}

export const poolData = {
  UserPoolId: userPoolId,
  ClientId: clientId,
};