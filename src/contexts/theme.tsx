//Libraries
import React, { createContext } from "react";

import { Colors, light, dark } from "../constants/colors";

interface Theme {
  light: Colors;
  dark: Colors;
}

interface ThemeContextProviderProps {
  children: JSX.Element;
}

const theme: Theme = {
  light: light,
  dark: dark,
};

export const ThemeContext = createContext(theme.light);

const ThemeContextProvider: React.FC<ThemeContextProviderProps> = ({ children }) => {
  return (
    <ThemeContext.Provider value={theme.light}>{children}</ThemeContext.Provider>
  );
};

export default ThemeContextProvider;