//Libraries
import React, { useState } from "react";

//Components
import {
  Center,
  Heading,
  Text,
  VStack,
  ButtonGroup,
  Button,
  useDisclosure
} from "@chakra-ui/react";
import { useNavigate } from "react-router";

//Hooks
import { useContext } from "react";

//Context
import { ThemeContext } from "../../../contexts/theme";

//Cognito
import { CognitoUserPool, CognitoUserAttribute } from 'amazon-cognito-identity-js';


import { Formik, Form } from "formik";
import * as Yup from "yup";
import { IoArrowBack } from "react-icons/io5";
import { poolData } from "../../../constants/poolData"
import TextField from "../TextField/TextField";
import Confirmation from "../Confirmation/Confirmation";


interface SignupProps { }

const Signup: React.FC<SignupProps> = () => {
  const theme = useContext(ThemeContext);
  const navigate = useNavigate();

  // Modal

  const [isShow, setIsShow] = useState(false);

  const handleConfirm = () => {
    setIsShow(false);
  }

  return (
    <Center h="90%" px={6} flexDirection="column" gap={6}>
      <Formik
        initialValues={{ firstname: "", lastname: "", email: "", password: "" }}
        validationSchema={Yup.object({
          firstname: Yup.string()
            .required("Prénom requis !"),
          lastname: Yup.string()
            .required("Nom requis !"),
          email: Yup.string()
            .required("Adresse mail requis !")
            .email("Entrez une addresse mail valide"),
          password: Yup.string()
            .required("Mot de passe requis !")
            .min(6, "Mot de passe trop court !")
            .max(30, "Mot de passe trop long"),
        })}
        onSubmit={(values, actions) => {

          const vals = { ...values }

          fetch("http://localhost:3001/auth/register", {
            method: "POST",
            credentials: "include",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(vals),
          }).catch(err => {
            return;
          }).then(res => {
            if (!res || !res.ok || res.status >= 400) {
              return;
            } else {
              return res.json();
            }
          }).then (data => {
            if(!data) return;
            console.log(data);
          });


          const userPool = new CognitoUserPool(poolData);

          const nameAttribute = new CognitoUserAttribute({
            Name: 'name',
            Value: values.firstname,
          });


          const givenNameAttribute = new CognitoUserAttribute({
            Name: 'given_name',
            Value: values.lastname,
          });


          userPool.signUp(values.email, values.password, [nameAttribute, givenNameAttribute], [], (err, result) => {
            if (err) {
              console.error(err);
            } else {
              setIsShow(true)
              console.log(result);
            }

          });

          actions.resetForm();
        }}
      >
        <VStack as={Form} w={{ base: "90%", md: "500px" }} spacing="1rem">

          <Heading size="xl" as="h2" color={theme.primary}>
            Inscription
          </Heading>

          <TextField name="firstname" placeholder="Prénom" autoComplete="off" />
          <TextField name="lastname" placeholder="Nom" autoComplete="off" />
          <TextField name="email" placeholder="Email" autoComplete="off" />
          <TextField name="password" placeholder="Mot de passe" autoComplete="off" type="password" />

          <ButtonGroup pt="1rem" >
            <Button colorScheme="teal" type="submit" >Créer un compte</Button>
            <Button onClick={() => navigate("/login")} leftIcon={<IoArrowBack />}>Retour</Button>
          </ButtonGroup>
        </VStack>
      </Formik>

      {/* Modal */}
      <Confirmation isShow={isShow} onConfirm={handleConfirm} />
    </Center >
  )
}

export default Signup;
