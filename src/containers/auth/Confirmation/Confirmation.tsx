import React, { useState } from "react";

import {
    Center,
    VStack,
    ButtonGroup,
    Button,
    useDisclosure,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
} from "@chakra-ui/react";

import { CognitoUserPool, CognitoUser } from 'amazon-cognito-identity-js';
import { poolData } from "../../../constants/poolData"
import { Formik, Form } from "formik";
import * as Yup from "yup";
import TextField from "../TextField/TextField";
import { useNavigate } from "react-router";


interface ConfirmationProps {
    isShow: boolean;
    onConfirm: () => void;
  }

const Confirmation: React.FC<ConfirmationProps> = ({isShow, onConfirm}) => {

    // Modal
    const { isOpen, onOpen, onClose } = useDisclosure()
    const navigate = useNavigate();
 
    return (
        <Center>
            <Formik
                initialValues={{ email: "", code: "" }}
                validationSchema={Yup.object({
                    email: Yup.string()
                        .required("Adresse mail requis !")
                        .email("Entrez une addresse mail valide"),
                    code: Yup.string()
                        .required("Code de confirmation requis")
                })}
                onSubmit={(values, actions) => {

                    const userPool = new CognitoUserPool(poolData);

                    const user = new CognitoUser({
                        Username: values.email,
                        Pool: userPool
                    });

                    user.confirmRegistration(values.code, true, (err, result) => {
                        if (err) {
                            console.error(err);
                            return;
                        } else {
                            console.log(result);
                            onConfirm();
                            navigate('/')
                        }
                    });
                    actions.resetForm();

                }}
            >
                <Modal closeOnOverlayClick={false} isOpen={isShow} onClose={onClose}>
                    <VStack as={Form} >
                        <ModalOverlay />
                        <ModalContent>
                            <ModalHeader>Code de confirmation</ModalHeader>
                            <ModalCloseButton />
                            <ModalBody pb={6}>
                                Veuillez renseignez votre email ainsi que le code qui vous a été envoyé par mail :
                                <TextField name="email" placeholder="Email" autoComplete="off" />
                                <TextField name="code" placeholder="Code de confirmation" autoComplete="off" />
                            </ModalBody>

                            <ModalFooter>
                                <ButtonGroup>
                                    <Button colorScheme="teal" type="submit" mr={3} >Confirmation</Button>
                                    <Button onClick={onClose}>Cancel</Button>
                                </ButtonGroup>
                            </ModalFooter>
                        </ModalContent>
                    </VStack>
                </Modal>
            </Formik>
        </Center>
    )
}

export default Confirmation;