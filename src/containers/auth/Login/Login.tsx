//Libraries
import React, { useState } from "react";

//Components
import { Center, Heading, Text, VStack, ButtonGroup, Button } from "@chakra-ui/react";
import { useNavigate } from "react-router";
import { Formik, Form } from "formik";
import * as Yup from "yup";

//Theme
import { ThemeContext } from "../../../contexts/theme";
import { useContext } from "react";
import TextField from "../TextField/TextField";

//Cognito
import { CognitoUserPool, CognitoUser, AuthenticationDetails } from 'amazon-cognito-identity-js';
import { poolData } from "../../../constants/poolData"

interface LoginProps { }


const Login: React.FC<LoginProps> = () => {
  const theme = useContext(ThemeContext);
  const navigate = useNavigate();

  return (
    <Center h="90%" px={6} flexDirection="column" gap={6}>
      <Formik
        initialValues={{ email: "", password: "" }}
        validationSchema={Yup.object({
          email: Yup.string()
            .required("Adresse mail requise !")
            .email("Entrez une addresse mail valide"),
          password: Yup.string()
            .required("Mot de passe requis !")
            .min(6, "Mot de passe trop court !")
            .max(30, "Mot de passe trop long"),
        })}
        onSubmit={(values, actions) => {

          const vals = { ...values }
          

          const userPool = new CognitoUserPool(poolData);

          const user = new CognitoUser({
            Username: values.email,
            Pool: userPool,
          });

          localStorage.setItem("username", values.email);

          const authDetails = new AuthenticationDetails({
            Username: values.email,
            Password: values.password,
          });

          user.authenticateUser(authDetails, {
            onSuccess: (data) => {
              console.log("onSucces :", data);
              navigate("/")
            },
            onFailure: function (err) {
              console.error("onFailure : ", err);
            },
            newPasswordRequired: (data) => {
              console.log("newPasswordRequired : ", data);
            }
          });
          actions.resetForm();
          fetch("http://localhost:3001/auth/login", {
            method: "POST",
            credentials: "include",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(vals),
          }).catch(err => {
            return;
          }).then(res => {
            if (!res || !res.ok || res.status >= 400) {
              return;
            } else {
              return res.json();
            }
          }).then (data => {
            if(!data) return;
            console.log(data);
          });
        }}
      >
        <VStack as={Form} w={{ base: "90%", md: "500px" }} spacing="1rem">

          <Heading size="xl" as="h2" color={theme.primary}>
            Connexion
          </Heading>

          <TextField name="email" placeholder="Email" autoComplete="off" />
          <TextField name="password" placeholder="Mot de passe" autoComplete="off" type="password" />

          <ButtonGroup pt="1rem" >
            <Button colorScheme="teal" type="submit" >Connexion</Button>
            <Button onClick={() => navigate("/signup")} >Créer un compte</Button>
          </ButtonGroup>
        </VStack>
      </Formik>
    </Center>
  );
};
export default Login;
