import React from "react";
import { Field, FieldProps, useField } from "formik"
import { FormControl, FormErrorMessage, FormLabel, Input } from "@chakra-ui/react";



const TextField = ({ label, ...props }:any) => {
    const [field, meta] = useField(props)
    return (
        <FormControl isInvalid={Boolean(meta.touched && meta.error)} >
            <FormLabel>{label}</FormLabel>
            <Input
                as={Field}
                {...field}
                {...props}
            >
            </Input>
            <FormErrorMessage>{meta.error}</FormErrorMessage>

        </FormControl >
    )
}

export default TextField;