//Libraries
import React, { ChangeEvent } from "react";

//Components
import {
  Input,
  Flex,
  Textarea,
  FormControl,
  FormLabel,
  Button,
  Text,
  Center,
} from "@chakra-ui/react";

import { Controller } from "react-hook-form";
import DownloadFile from "../../components/Download";

//Hooks
import { useState, useContext, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router";

//Icons
import { IoAddCircleSharp } from "react-icons/io5";

//Services
import { uploadToS3 } from "../../services/s3/upload";

//Contexts
import { ThemeContext } from "../../contexts/theme";

interface AddPostProps {}

const AddPost: React.FC<AddPostProps> = () => {
  //States
  const [filekey, setFileKey] = useState("");
  const [fileName, setFileName] = useState("");

  const navigate = useNavigate();

  //Form
  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm();

  //Theme
  const theme = useContext(ThemeContext);

  //Methods
  const handleChangeValue = (e: any) => {
    setFileName(e.target.value);
  };

  const onSubmit = async (data: any) => {
    const uploadedKey = await uploadToS3(data);
    setFileKey(uploadedKey);
    localStorage.setItem("file", uploadedKey);
    navigate("/");
  };

  return (
    <>
      <form style={{ height: "100%" }} onSubmit={handleSubmit(onSubmit)}>
        <Center flexDirection="column" p={6} h="100%" gap={4}>
          <FormControl>
            <FormLabel htmlFor="file" m={0} display="flex" justifyContent="center">
              <IoAddCircleSharp size="80px" color={theme.primary} />
            </FormLabel>

            <Input
              {...register("file", {
                onChange: handleChangeValue,
              })}
              display="none"
              type="file"
              accept="image/jpeg image/png"
              id="file"
            />
          </FormControl>

          <Text mb={14}>Choissisez le média que vous souhaitez publier !</Text>

          <Text>{fileName}</Text>

          <Textarea
            {...register("description")}
            placeholder="Écrivez la légende de votre publication..."
          />

          <Button
            mt={4}
            backgroundColor={theme.primary}
            color={theme.background}
            type="submit"
          >
            Publier
          </Button>
        </Center>
      </form>
    </>
  );
};

export default AddPost;
