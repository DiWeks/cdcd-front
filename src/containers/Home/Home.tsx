//Libraries
import React from "react";

//Components
import PicturePost from "../../components/ui/posts/PicturePost/PicturePost";
import TwitPost from "../../components/ui/posts/TweetPost/TweetPost";
import DownloadFile from "../../components/Download";

interface HomeProps {}

const Home: React.FC<HomeProps> = () => {
  const filekey = localStorage.getItem("file");
  return (
    <>
      <PicturePost filekey={filekey} />
      <PicturePost filekey={filekey} />
      <PicturePost filekey={filekey} />
      <PicturePost filekey={filekey} />
      <PicturePost filekey={filekey} />
      <PicturePost filekey={filekey} />
      <TwitPost />
    </>
  );
};

export default Home;
