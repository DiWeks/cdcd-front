//Libraries
import React from "react";

//Components
import {
  Flex,
  Box,
  Text,
  Input,
  InputGroup,
  InputLeftElement,
  VStack,
} from "@chakra-ui/react";

import FriendCard from "../../components/ui/cards/FriendCard/FriendCard";
import FriendSuggestCard from "../../components/ui/cards/FriendSuggestCard/FriendSuggestCard";
import FriendRequestCard from "../../components/ui/cards/FriendRequestCard/FriendRequestCard";

//Hooks
import { useContext, useState } from "react";

//Contexts
import { ThemeContext } from "../../contexts/theme";

//Icons
import { IoSearchOutline } from "react-icons/io5";

const Friends = () => {
  const [view, setView] = useState("friends");
  const theme = useContext(ThemeContext);
  return (
    <Box>
      <InputGroup>
        <InputLeftElement py={6} pointerEvents="none" children={<IoSearchOutline />} />
        <Input
          type="text"
          placeholder="Rechercher"
          py={6}
          borderRadius={0}
          _focus={{
            borderColor: theme.primary,
          }}
        />
      </InputGroup>

      <Flex h="50px" borderBottomWidth={1}>
        <Flex flex={1} justify="center" align="center" onClick={() => setView("friends")}>
          <Text color={view === "friends" ? theme.primary : theme.text}>Amis</Text>
        </Flex>
        <Flex
          borderLeftWidth={1}
          borderRightWidth={1}
          flex={1}
          justify="center"
          align="center"
          onClick={() => setView("requests")}
        >
          <Text color={view === "requests" ? theme.primary : theme.text}>Demandes</Text>
        </Flex>
        <Flex
          flex={1}
          justify="center"
          align="center"
          onClick={() => setView("suggestions")}
        >
          <Text color={view === "suggestions" ? theme.primary : theme.text}>
            Suggestions
          </Text>
        </Flex>
      </Flex>

      <Box>
        {view === "friends" && (
          <VStack p={4} spacing={4}>
            <FriendCard />
            <FriendCard />
            <FriendCard />
            <FriendCard />
            <FriendCard />
            <FriendCard />
            <FriendCard />
            <FriendCard />
            <FriendCard />
            <FriendCard />
            <FriendCard />
            <FriendCard />
          </VStack>
        )}

        {view === "requests" && (
          <VStack p={4} spacing={4}>
            <FriendRequestCard />
            <FriendRequestCard />
            <FriendRequestCard />
            <FriendRequestCard />
            <FriendRequestCard />
            <FriendRequestCard />
            <FriendRequestCard />
            <FriendRequestCard />
            <FriendRequestCard />
            <FriendRequestCard />
            <FriendRequestCard />
          </VStack>
        )}

        {view === "suggestions" && (
          <VStack p={4} spacing={4}>
            <FriendSuggestCard />
            <FriendSuggestCard />
            <FriendSuggestCard />
            <FriendSuggestCard />
            <FriendSuggestCard />
            <FriendSuggestCard />
            <FriendSuggestCard />
            <FriendSuggestCard />
            <FriendSuggestCard />
            <FriendSuggestCard />
            <FriendSuggestCard />
          </VStack>
        )}
      </Box>
    </Box>
  );
};

export default Friends;
