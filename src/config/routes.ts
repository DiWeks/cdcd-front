const routes = {
  HOME: "/",
  SIGNUP: "/signup",
  LOGIN: "/login",
  FRIENDS: "/friends",
  ADD_POST: "/add-post",
  CHAT_LIST: "/chat-list",
  CHAT_ROOM: "/chat-list/:id",
  PROFILE: "/profile",
}

export default routes;