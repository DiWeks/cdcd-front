FROM node:18 AS builder

WORKDIR /usr/frontend/src/app

COPY package*.json ./

RUN yarn install

COPY . .

RUN yarn build



FROM nginx:alpine


# Dossier de base que va chercher nginx: /usr/share/nginx/html
COPY --from=builder /usr/frontend/src/app/build /usr/share/nginx/html

EXPOSE 80
